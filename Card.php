<?php

/**
 *  A card contains a vehicle and the source and destinaton of each subtrip 
 */
require_once ('Transportation/Vehicle.php');

class Card {

  /**
   * [$vehicle description]
   *
   * @var [type]
   */
   private $vehicle;

  /**
   * [$vehicle description]
   *
   * @var [type]
   */
   private $departure;
  
  /**
   * [$vehicle description]
   *
   * @var [type]
   */
   private $arrival;
	
  /**
   * [Constructor initialize values]
   */	
   public function __construct ($vehicle, $departure, $arrival) {
		$this->vehicle = $vehicle;
		$this->departure = $departure;
		$this->arrival = $arrival;
	}

  /**
   * [setDeparture description]
   *
   * @param [type] $name [description]
   */
	public function setDeparture ($departure){
		$this->departure = $departure;
	}

  /**
   * [getDeparture description]
   *
   * @return [type] [description]
   */
	public function getDeparture () {
		return $this->departure;
	}

  /**
   * [setArrival description]
   *
   * @param [type] $name [description]
   */
  public function setArrival ($seat){
    $this->arrival = $arrival;
  }

  /**
   * [getArrival description]
   *
   * @return [type] [description]
   */
  public function getArrival () {
  
    return $this->arrival;
  } 	

  /**
   * Print info behind each card according to the type of the vehivle 
   * and the cahracteristic passed for each subtrip 
   */
  public function printMessage ($cards) {
	foreach($cards as $card)
		switch ($card->vehicle->getTransportationType()) {
			case 'Bus' :
      			echo "Take the airport bus from $card->departure to $card->arrival.", 
      			($card->vehicle->getTransportationSeat() == null ? " No seat assignment.<br> <br> \n\n" : " Sit in seat ". $card->vehicle->getTransportationSeat() . ".<br> <br>\n\n");
					break;
			case 'Plane' :
				echo "From $card->departure, take flight ". $card->vehicle->getFLight() . " to $card->arrival. Gate " .$card->vehicle->getGate() . 
				", seat " . $card->vehicle->getTransportationSeat() . ".<br>\n", 
				($card->vehicle->getBaggageCounter() == null ? "Baggage will we automatically transferred from your last leg.\n\n" : 
				"Baggage drop at ticket counter ". $card->vehicle->getBaggageCounter() . ".<br> <br>\n\n"); ; 
				break;
			case 'Train' :
				echo "Take train ".$card->vehicle->getTrainName() ." from $card->departure to $card->arrival. Sit in seat  ". $card->vehicle->getTransportationSeat() . ".<br> <br>\n\n" ;
				break;								
			default : 
				echo "No such vehicle yet --loading--\n<br>";
		}
	}
}