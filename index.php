<?php

/**
 * main class that initiate the whole program
 */

require_once('Transportation/Bus.php');
require_once('Transportation/Plane.php');
require_once('Transportation/Train.php');
require_once('Card.php');
require_once('CustomList.php');
require_once('Trip.php');

$vehicle1 = new Train('78A','45B');
$vehicle2 = new bus();
$vehicle3 = new Plane('SK455', '45B', '3A', '344');
$vehicle4 = new Plane('SK22', '22', '7B');


$card1 = new Card($vehicle1, 'Madrid', 'Barcelona');
$card2 = new Card($vehicle2, 'Barcelona', 'Gerona Airport');
$card3 = new Card($vehicle3, 'Gerona Airport', 'Stockholm');
$card4 = new Card($vehicle4, 'Stockholm','New York JFK');


echo "<br>-------------------Trip One---------------------------\n<br>";
$trip = new Trip();
$trip->bulkAdd(array($card2,$card1,$card3,$card4));
$trip->initialize();
echo "<br>--------------------Trip Two--------------------------\n<br>";
$trip = new Trip();
$trip->bulkAdd(array($card3,$card1,$card2,$card4));
$trip->initialize();
echo "<br>---------------------Trip Three-------------------------\n<br>";
$trip = new Trip();
$trip->bulkAdd(array($card2,$card1,$card3,$card4));
$trip->initialize();
echo "<br>----------------------Trip Four------------------------\n<br>";
$trip = new Trip();
$trip->bulkAdd(array($card4,$card2,$card3,$card1));
$trip->initialize();
echo "<br>-----------------------Trip Five-----------------------\n<br>";
$trip = new Trip();
$trip->bulkAdd(array($card3,$card4,$card1,$card2));
$trip->initialize();
echo "<br>-----------------------Trip Six-----------------------\n<br>";
$trip = new Trip();
$trip->bulkAdd(array($card3,$card2,$card4));
$trip->initialize();

