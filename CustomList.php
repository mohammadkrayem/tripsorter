<?php

/**
 *  Custom List that will manage where the passed card will be added to
 */

class CustomList {

 /**
  * [$result To be filled with the final result after sorting is done]
  *
  * @return [type] []
  */
 public $result = array();
 
 /**
  * [$preparttionList To be filled with chunks of sorted picees as cards are passed one by one]
  *
  * @return [type] [Eventually it will be an array of sorted arrays or one big sorted array (depends on tghe case)]
  */	
	public $preparationList = array();
		
	public function __construct () {}
 
 /**
  * [$index] [parameter1 index of sub array in preparationList]
  * [$item]  [parameter2 object (card) that will be added at the begining of this subarray]
  *	[$pos]   [parameter3 position where this card will be added ( at the begining so will always be passed 0)]
  *
  * @return [type] []
  */	
	public function prePush ($index, $item, $pos) {
		array_splice($this->preparationList[$index], $pos, 0 , array($item));
	}

 /**
  * [$index] [parameter1 index of sub array in preparationList]
  * [$item]  [parameter2 object (card) that will be added at the end of this subarray]
  *
  * @return [type] []
  */	
	public function postPush ($index, $item) {
		array_push($this->preparationList[$index], $item);	
	}

	public function toString () {
		echo "<pre>";
		var_dump($this->result);
	}

}
