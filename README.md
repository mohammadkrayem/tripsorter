# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

You are given a stack of boarding cards for various transportations that will take you from a point A to point B via several stops on the way. All of the boarding cards are out of order and you don't know where your journey starts, nor where it ends. Each boarding card contains information about seat assignment, and means of transportation (such as flight number, bus number etc).

### How do I get set up? ###

-Clone the repository and add it to the root directory of your apache web server(or any other web server you use)

-Open your Terminal(bash) and go to the repository directory and run index.php(or you can open you web browser and browse to index.php in the downloaded repository

-There will be 6 outputs for 6 trips already generated in index.php with cards added in a random manner(not sorted)

-You may add to index.php a new trip with your scenario of cards that you would like to test and check the output accordingly.

Code Hierarchy:

Transportation Folder:

 -Vehicle ( An abstraction class to the means of transportation)

 -Bus, Train, and plane inherits from Vehicle.

Card Class:

 -A card contains a vehicle, departure and arrival.

Trip Class:

 -A trip is made of many cards. This class sorts the cards.

CustomList:

 -A custom data structure that helps in sorting the cards.

index Class:

 -To run the program.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact