<?php 

/**
 * Trip that will be:
 * Adding cards to $cards array
 * Sorting the cards by producing a chunk of seperate sorted cards stored in $preparationList
 * Then sorting the preparationList to arrive to the final result that will be stored in $result  
 */
require_once('Transportation/Bus.php');
require_once('Transportation/Plane.php');
require_once('Transportation/Train.php');
require_once('Card.php');
require_once('CustomList.php');

class Trip {

/**
 * [$cards To be filled with the unsorted cards]
 *
 * @return [type] []
 */
 public $cards = array();

/**
 * [$cards To be filled with the unsorted cards]
 *
 * @return [type] []
 */ 
 public $counter = 0;

/**
 * [$cards To be filled with the unsorted cards]
 *
 * @return [type] []
 */ 
 public $max_loop;

/**
 * [$cards To be filled with the unsorted cards]
 *
 * @return [type] []
 */ 
 public $tripList;

 public function __construct () {
   $this->tripList = new CustomList();
 }
 /**
  * [$card current passed card to be added to $cards]
  *
  * @return  []
  */
  public function addCard ($card) {    
     array_push($this->cards, $card);
  }
 
 /**
  * [$cards array of cards -> will take each card and add it to $cards]
  *
  * @return [type] []
  */
  public function bulkAdd ($cards) {
      foreach ($cards as $card) {
         $this->addCard($card);
      }
  }

 /**
  * Takes the current card in the cards array(unsorted) and pass it to manageCurrentCard function
  * When manageCurrentCard is done we will end up with preparationList array 
  * preparationList is an array with sorted arrays or could have only 1 array all sorted depends on the case
  * If preparationList has more than one array then those sub arrays need to be sorted =>  finalize function will be called
  * otherwise it has one sorted arrays and PrintCardMessage function will be called direclty
  * @return [type] []
  */
  public function initialize () {
      foreach ($this->cards as $card) {
         $this->ManageCurrentCard($card);
      }
      // you can uncomment this and view how preparationList looks like when satge one is complete
      // print_r($this->tripList->preparationList);

      $this->max_loop = count($this->tripList->preparationList);
      if (count($this->tripList->preparationList) > 1 ) {         
         while (count($this->tripList->result) !== count($this->cards)) { 
            /* if the counter exceeded the cpunt of preparation list (count of sub arrays)
             * Then there is definetly either a broken link or dubplicate card
             */ 
            if($this->max_loop >= $this->counter) {        
               $this->finalize();
               $this->counter++;
            } 
            else {
               echo "<br>Error: There is a Broken link or Duplicare cards<br>";
               return;
            }
         }    
      } else 
         $this->tripList->result = $this->tripList->preparationList[0];
         
      $this->printCardMessage();
   }
 
 /**
  * Takes the current card and checks its departure and arrival accros the end points of the sub array 
  * arrival and departure. no need to loop over the whole subarrays cards since in the middle are definetly sorted
  * if it the current card has a link woth one of the endpoints of sub arrays then pre push or postpush is called 
  * otherwise it will added to a new subarray of the preparationList array
  * @return [type] []
  */
   public function ManageCurrentCard ($card) {
       $isFound = false;
       foreach ($this->tripList->preparationList as $currentListIndex => $currentList) {
            if(count($currentList) > 0) {
               if ($card->getArrival() == $currentList[0]->getDeparture()) {            
                  $this->tripList->prePush($currentListIndex, $card, 0);
                  $isFound= true;
                  break;
               }               
            elseif ($card->getDeparture() == $currentList[count($currentList)-1]->getArrival() ) {
               $this->tripList->postPush($currentListIndex, $card,count($currentList)-1);
               $isFound= true;
               break;
            }
         }
      }
      if (!$isFound) {  
         array_push($this->tripList->preparationList, array($card));
      }
   }

 /**
  * This method is called when subarrays exits in preparationList
  * It will swap the postion of the sub arrays and add them to result array
  * @return [type] []
  */
   public function finalize()
   {
      // will hit only one time to check the dep and arrival of the first subarray endpoint along with the other subarrays endpoints
      // o(n)
      if(count($this->tripList->result) == 0) {
         for($j = 1; $j < count($this->tripList->preparationList); $j++) {
            if ($this->tripList->preparationList[0][0]->getDeparture() == $this->tripList->preparationList[$j][count($this->tripList->preparationList[$j])-1]->getArrival() ) {             
               $this->tripList->result = array_merge($this->tripList->preparationList[$j], $this->tripList->preparationList[0]);
               break;
            } 
            elseif ($this->tripList->preparationList[0][count($this->tripList->preparationList[0])-1]->getArrival() == $this->tripList->preparationList[$j][0]->getDeparture() ) {
               $this->tripList->result = array_merge($this->tripList->preparationList[0], $this->tripList->preparationList[$j]);
               break;            
            }           
         }
      }// will hit to check the newly result array along witht the other sub arrays o(n)
      elseif (count($this->tripList->result) > 0) {
         for($j = 1; $j < count($this->tripList->preparationList); $j++) {
            if ($this->tripList->result[0]->getDeparture() == $this->tripList->preparationList[$j][count($this->tripList->preparationList[$j])-1]->getArrival()) {
               $this->tripList->result = array_merge($this->tripList->preparationList[$j],$this->tripList->result);
               break;
            } 
            elseif ($this->tripList->result[count($this->tripList->result)-1]->getArrival() == $this->tripList->preparationList[$j][0]->getDeparture()) {            
               $this->tripList->result = array_merge($this->tripList->result,$this->tripList->preparationList[$j]);
               break;
            }
         }            
      }
   }
  /**
   * calls the printMesage in the card to view the info behind the card 
   */
   public function printCardMessage () {
      $card = new ReflectionClass('Card');
      $cardRef = $card->newInstanceWithoutConstructor();        
      $cardRef->printMessage ($this->tripList->result);    
   }
}