<?php

/**
 * Train as a mean of teransportaion inherits the characteristic of the vehicle class
 */
require_once('Vehicle.php');

class Train extends Vehicle {

  /**
   * [$trainName description]
   *
   * @var [type]
   */
  private $trainName;

	/**
	 * [Constructor initialize values]
	 */
	public function __construct ($seat, $trainName) {  
    parent::__Construct('Train', $seat);
    $this->trainName = $trainName;
	}

  /**
   * [getTrainName description]
   *
   * @return [type] [description]
   */
  public function getTrainName () {
    return $this->trainName;
  }
}