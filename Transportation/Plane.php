<?php

/**
 * plane as a mean of teransportaion inherits the characteristic of the vehicle class
 */
require_once("Vehicle.php");

class Plane extends Vehicle {

  /**
   * [$flight description]
   *
   * @var [type]
   */
  private $flight;

  /**
   * [$gate description]
   *
   * @var [type]
   */
  private $gate;

  /**
   * [$baggageCounter description]
   *
   * @var [type]
   */
  private $baggageCounter;  

	/**
	 * [Constructor initialize values]
	 */
	public function __construct ($flight, $gate, $seat, $baggageCounter = null) {  
    parent::__Construct('Plane', $seat);
    $this->flight = $flight;
    $this->gate = $gate;
    $this->baggageCounter = $baggageCounter;
	}

  /**
   * [setFlight description]
   *
   * @param [type] $flight [description]
   */
  public function setFlight ($flight) {
    $this->flight = $flight;
  }

  /**
   * [getFlight description]
   *
   * @return [type] [description]
   */
  public function getFlight () {
    return $this->flight;
  }

  /**
   * [setGate description]
   *
   * @param [type] $gate [description]
   */
  public function setGate($gate) {
    $this->gate = $gate;
  }

  /**
   * [getGate description]
   *
   * @return [type] [description]
   */
  public function getGate () {
    return $this->gate;
  }    

  /**
   * [setBaggageCounter description]
   *
   * @param [type] $baggageCounter [description]
   */
  public function setBaggageCounter ($baggageCounter) {
    $this->baggageCounter = $baggageCounter;
  }

  /**
   * [getBaggageCounter description]
   *
   * @return [type] [description]
   */
  public function getBaggageCounter () {
    return $this->baggageCounter;
  }

}