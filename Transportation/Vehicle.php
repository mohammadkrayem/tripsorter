<?php

/**
 * Transportation Abstraction Class
 */
abstract class Vehicle {

  /**
   * [$seat description]
   *
   * @var [type]
   */
	private $seat;

  /**
   * [$type description]
   *
   * @var [type]
   */  
  private $type;
	
  /**
	 * [Constructor initialize values]
	 */
	public function __construct ($type, $seat) {
		$this->seat = $seat;
    $this->type = $type;
	}

  /**
   * [setTransportationSeat description]
   *
   * @param [type] $name [description]
   */
	public function setTransportationSeat ($seat) {
		$this->seat = $seat;
	}

  /**
   * [getTransportationSeat description]
   *
   * @return [type] [description]
   */
	public function getTransportationSeat () {
		return $this->seat;
	}

  /**
   * [setTransportationType description]
   *
   * @param [type] $name [description]
   */
  public function setTransportationType ($seat) {
    $this->type = $type;
  }

  /**
   * [getTransportationType description]
   *
   * @return [type] [description]
   */
  public function getTransportationType () {
    return $this->type;
  }     

}