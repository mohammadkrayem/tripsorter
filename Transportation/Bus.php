<?php

/**
 * Bus as a mean of teransportaion inherits the characteristic of the vehicle class
 */
require_once('Vehicle.php');

class Bus extends Vehicle {

	/**
	 * [Constructor initialize values]
	 */
	public function __construct ($seat = null) {  
    	parent::__construct('Bus',$seat);
	}
}